package com.atlassian.jira.plugins.mailqueueapi.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatsBean {

	private final int queueSize;
	private final int errorQueueSize;

	public StatsBean(final int queueSize, final int errorQueueSize) {
		this.queueSize = queueSize;
		this.errorQueueSize = errorQueueSize;
	}

	@XmlElement
	public int getQueueSize() {
		return queueSize;
	}

	@XmlElement
	public int getErrorQueueSize() {
		return errorQueueSize;
	}
}
